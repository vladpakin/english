import path from 'path';

const index = path.resolve('./index.html');

module.exports = (app) => {
	app.route('/')
		.get((req, res, next) => {
			return res.sendFile(index)
		})
};