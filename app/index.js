import listen from '../config/express';
import dbConnect from '../config/db';

export function start() {
	listen();
	/* TODO: add connect to postgresql connection config and connect to db before start server.*/
	// dbConnect()
	// 	.on('error', onError)
	// 	.on('disconnect', dbConnect)
	// 	.once('open', );
};

const onError = (error) => {
	console.log(error);
	process.exit(-1);
};