import path from 'path';

import express from 'express'
import bodyParser from 'body-parser';
import morgan from 'morgan';
import chalk from 'chalk';
import methodOverride  from 'method-override';

import * as config from './config';
import * as utils from './utils';

export default function listen() {
	configureApp().listen(config.port);

	console.log('--');
	console.log(chalk.green('Environment:\t\t\t' + process.env.NODE_ENV));
	console.log(chalk.green('Port:\t\t\t\t' + config.port));
	console.log(chalk.green('Database:\t\t\t' + config.db.uri));
	console.log('--');
};

const configureApp = () => {
	var app = express();

	app.use('/api/v1/', function (req, res, next) {
		/* TODO : write authorization checker for this routes*/
		next();
	});

	app.use(function (req, res, next) {
		res.locals.host = req.protocol + '://' + req.hostname;
		res.locals.url  = req.protocol + '://' + req.headers.host + req.originalUrl;
		next();
	});

	app.set('showStackError', true);
	app.use(morgan('dev'));
	app.use(bodyParser.urlencoded({
		extended: true
	}));
	app.use(bodyParser.json());
	app.use(methodOverride());

	utils.getGlobbedPaths('./app/routes/**/*.js')
		.forEach(function (route) {
			require(path.resolve(route))(app)
		});

	return app;
};


