var fs = require('fs');

var babelrc = fs.readFileSync('./.babelrc');
var config;

try {
	config        = JSON.parse(babelrc);
	config.ignore = /^(?=.*?\bnode_modules\b)((?!compomatic-service-core).)*$/;
} catch (err) {
	console.error('==>     ERROR: Error parsing your .babelrc.');
	console.error(err);
}

require('babel-core/register')(config);
